package edu.pdx.cs410.wifi.direct.file.transfer;

import android.Manifest;
import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class GPSServiceActivity extends Activity implements SensorEventListener{

    private Button btn_start, btn_stop;
    private TextView textView, textView2, textView3, textView4;
    private BroadcastReceiver broadcastReceiver;
    private BroadcastReceiver broadcastReceiverNetwork;

    //To save data
    private FileOutputStream fileOutputStreamGPS;
    private FileOutputStream fileOutputStreamGyro;
    private FileOutputStream fileOutputStreamAccel;
    //private FileOu

    //To add sensor data -- Gyroscope
    private Sensor gyroSensor;
    private Sensor accelSensor;
    private SensorManager gyrosensorManager;
    private SensorManager accelSensorManager;

    @Override
    protected void onResume() {

        accelSensorManager.registerListener(this, accelSensor, SensorManager.SENSOR_DELAY_NORMAL);
        gyrosensorManager.registerListener(this,gyroSensor, SensorManager.SENSOR_DELAY_NORMAL);

        super.onResume();
        if (broadcastReceiver == null){
            broadcastReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String Data = "\nLongitude: " + intent.getExtras().get("longitude") +
                            "\nLatitude: " + intent.getExtras().get("latitude")+
                            "\nAltitude: " + intent.getExtras().get("altitude")+
                            "\nAccuracy: " + intent.getExtras().get("accuracy")+
                            "\nBearing: " + intent.getExtras().get("bearing")+
                            "\nSpeed: " + intent.getExtras().get("speed")+ "\n";
                    textView.setText(Data); //coordinates is the key

                    //try to save these data into GPS_Info file
                    try {
                        fileOutputStreamGPS.write(Data.getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            };
        }
        registerReceiver(broadcastReceiver, new IntentFilter("location_update"));

        if (broadcastReceiverNetwork == null){
            broadcastReceiverNetwork = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {

                    String DataNetwork = "\nLongitudeN: " + intent.getExtras().get("longitude_Network") +
                            "\nLatitudeN: " + intent.getExtras().get("latitude_Network")+
                            "\nAltitudeN: " + intent.getExtras().get("altitude_Network")+
                            "\nAccuracyN: " + intent.getExtras().get("accuracy_Network")+
                            "\nBearingN: " + intent.getExtras().get("bearing_Network")+
                            "\nSpeedN: " + intent.getExtras().get("speed_Network")+ "\n";
                    textView4.setText(DataNetwork);

                    //try to save these data into GPS_Info file
                    /*try {
                        fileOutputStreamGPS.write(Data.getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/
                }
            };
        }
        registerReceiver(broadcastReceiverNetwork, new IntentFilter("location_update_Network"));

    }

    @Override
    protected void onDestroy() {//kind of like a destructor
        super.onDestroy();
        if (broadcastReceiver != null){
            unregisterReceiver(broadcastReceiver);
        }
        if (broadcastReceiverNetwork != null){
            unregisterReceiver(broadcastReceiverNetwork);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gpsservice);

        btn_start = (Button) findViewById(R.id.button1);
        btn_stop = (Button) findViewById(R.id.button2);
        textView = (TextView) findViewById(R.id.textView);
        textView2 = (TextView) findViewById(R.id.textView2);
        textView3 = (TextView) findViewById(R.id.textView3);
        textView4 = (TextView) findViewById(R.id.textView4);

        //Create our sensor manager
        accelSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
        gyrosensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);

        //Accelerometer & Gyro sensors
        accelSensor = accelSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        gyroSensor = gyrosensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);

        //register sensor listeners
        accelSensorManager.registerListener(this, accelSensor, SensorManager.SENSOR_DELAY_NORMAL);//0.2secs delay interval
        gyrosensorManager.registerListener(this, gyroSensor, SensorManager.SENSOR_DELAY_NORMAL);


        //if (!runtime_permissions())
            enable_buttons();

    }

    private void enable_buttons() {

        btn_start.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                //create a file
                String state;
                state = Environment.getExternalStorageState();

                if (Environment.MEDIA_MOUNTED.equals(state)){

                    File Root = Environment.getExternalStorageDirectory();
                    File Dir = new File(Root.getAbsolutePath()+"/MyAppFile");
                    if (!Dir.exists()){
                        Dir.mkdir();
                    }
                    File fileGPS = new File(Dir, "MyGPS.txt");
                    File fileGyro = new File(Dir, "MyGyro.txt");
                    File fileAccel = new File(Dir, "MyAccelerometer.txt");
                    //File fileGPSNetwork = new File(Dir, "MyGPSNetwork.txt");
                    try {
                        fileOutputStreamGPS = new FileOutputStream(fileGPS);
                        fileOutputStreamGyro = new FileOutputStream(fileGyro);
                        fileOutputStreamAccel = new FileOutputStream(fileAccel);

                        Intent i = new Intent(getApplicationContext(), GPS_Service.class);
                        startService(i);


                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }


                }
                else{
                    Toast.makeText(getApplicationContext(), "SD card not found", Toast.LENGTH_SHORT).show();
                }



            }
        });

        btn_stop.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v) {

                //close the file
                try {
                    Intent i = new Intent(getApplicationContext(), GPS_Service.class);
                    stopService(i);


                    fileOutputStreamGPS.close();
                    fileOutputStreamAccel.close();
                    fileOutputStreamGyro.close();

                    Toast.makeText(getApplicationContext(), "Data Saved", Toast.LENGTH_SHORT).show();
                } catch (IOException e) {
                    e.printStackTrace();
                }

            }
        });

    }



    public void onSensorChanged(SensorEvent sensorEvent) {

        Sensor sensor = event.sensor;
        if (sensor.getType() == Sensor.TYPE_ACCELEROMETER){
            //String accelData = String.valueOf(event.values[0]);
            String accelData = "X: " + event.values[0] + "\n" + "Y: " + event.values[1] + "\n" + "Z: " + event.values[2] + "\n";
            textView2.setText(accelData);
            //textView.append(accelData);
            if (fileOutputStreamAccel != null){
                try {
                    fileOutputStreamAccel.write(accelData.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            /*try {
                fileOutputStreamAccel.write(accelData.getBytes());
            } catch (IOException e) {
                e.printStackTrace();
            }*/
        }
        else if (sensor.getType() == Sensor.TYPE_GYROSCOPE){

            String gyroData = "0: " + event.values[0] + "\n" + "1: " + event.values[1] + "\n" + "2: " + event.values[2] + "\n";
            textView3.setText(gyroData);

            if (fileOutputStreamGyro != null){
                try {
                    fileOutputStreamGyro.write(gyroData.getBytes());
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public void onAccuracyChanged(Sensor sensor, int i) {

    }

    @Override
    protected void onPause() {
        accelSensorManager.unregisterListener(this, accelSensor);
        gyrosensorManager.unregisterListener(this, gyroSensor);


        super.onPause();
    }
}
