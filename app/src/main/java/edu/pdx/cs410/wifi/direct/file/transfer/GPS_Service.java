package edu.pdx.cs410.wifi.direct.file.transfer;

import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;

import org.jetbrains.annotations.Nullable;

/**
 * Created by Mengyang on 2/17/2017.
 */
public class GPS_Service extends Service{
    private LocationListener listener;
    private LocationListener listenerNetwork;
    private LocationManager locationManager;
    private LocationManager lmNetwork;

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {

        listener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                Intent i = new Intent("location_update"); //Intent Filter
                i.putExtra("longitude", location.getLongitude());
                i.putExtra("latitude", location.getLatitude());
                i.putExtra("altitude", location.getAltitude());
                i.putExtra("accuracy", location.getAccuracy());
                i.putExtra("bearing", location.getBearing());
                i.putExtra("speed", location.getSpeed());
                sendBroadcast(i);

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        };

        //Network Provider
        listenerNetwork = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {

                Intent i = new Intent("location_update_Network"); //Intent Filter
                i.putExtra("longitude_Network", location.getLongitude());
                i.putExtra("latitude_Network", location.getLatitude());
                i.putExtra("altitude_Network", location.getAltitude());
                i.putExtra("accuracy_Network", location.getAccuracy());
                i.putExtra("bearing_Network", location.getBearing());
                i.putExtra("speed_Network", location.getSpeed());
                sendBroadcast(i);

            }

            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {

            }

            @Override
            public void onProviderEnabled(String provider) {

            }

            @Override
            public void onProviderDisabled(String provider) {

                Intent i = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
            }
        };
        locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 2, 0, listener);

        //network provider
        lmNetwork = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);

        lmNetwork.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 200, 0, listenerNetwork);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (locationManager != null){
            locationManager.removeUpdates(listener);
            lmNetwork.removeUpdates(listenerNetwork);
        }
    }
}
